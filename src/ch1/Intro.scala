package ch1

/**
  * Created by meras on 11/10/2016.
  */
object Intro {

  def main(args: Array[String]): Unit = {
    var capital =
      Map("US" -> "Washington"
        , "France" -> "Paris")

    capital += ("Japan" -> "Tokyo")

    println(capital("France"))
    capital.foreach(println)

    println(factorial(30))

  }

  def factorial(x: BigInt): BigInt =
    if (x == 0) 1 else x * factorial(x - 1)
}
