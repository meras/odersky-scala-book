package ch6

/**
  * Created by meras on 11/12/2016.
  */
class Rational(n: Int, d: Int) {
  require(d != 0)

  private val gcd: Int =gcd(n.abs, d.abs)
  val numerator: Int = n / gcd
  val denominator: Int = d / gcd

  def this(n: Int) = this(n, 1)

  def + (that: Rational): Rational = {
    new Rational(
      numerator * that.denominator + that.numerator * denominator,
      denominator * that.denominator
    )
  }

  def + (i: Int): Rational = {
    new Rational(
      numerator + i * denominator,
      denominator
    )
  }

  def - (that: Rational): Rational = {
    new Rational(
      numerator * that.denominator - that.numerator * denominator,
      denominator * that.denominator
    )
  }

  def - (i: Int): Rational = {
    new Rational(
      numerator - i * denominator,
      denominator
    )
  }

  def * (that: Rational): Rational = {
    new Rational(
      this.numerator * that.numerator,
      this.denominator * that.numerator
    )
  }

  def * (i: Int): Rational = {
    new Rational(
      numerator * i,
      denominator
    )
  }

  def / (that: Rational): Rational = {
    new Rational(
      numerator * that.denominator,
      denominator * that.numerator
    )
  }

  def / (i: Int): Rational = {
    new Rational(
      numerator,
      denominator * i
    )
  }
  
  override def toString = s"$numerator/$denominator"

  private def gcd(a: Int, b:Int): Int =
    if (b == 0) a else gcd(b, a % b)
}