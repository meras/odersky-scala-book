package ch3

import scala.collection.mutable


/**
  * Created by meras on 11/10/2016.
  */
object NextSteps {

  def main(args: Array[String]): Unit = {

    val greetStrings = new Array[String](3)
    greetStrings(0) = "Hello"
    greetStrings(1) = ", "
    greetStrings(2) = "world!\n"

    for (i <- 0 to 2) print(greetStrings(i))

    val oneTwo = List(1, 2)
    val threeFour = List(3, 4)
    //list concatanation op :::, creates a new list
    println(oneTwo ::: threeFour)

    println(1 :: 2 :: threeFour)

    println((1 to 10).toList.count(_ > 5))

    val pair = (99, "Problems")
    println(pair._1)
    println(pair._2)

    var jetSet = Set("Boeing", "Airbus")
    jetSet += "Lear"

    val movieSet = mutable.Set("Psycho", "Poltergeist")
    movieSet += "Shrek"
    print(movieSet)

    val treasureMap = mutable.Map[Int, String]()
    treasureMap += (1 -> "Go to island")
    treasureMap += (2 -> "Find big x on ground.")
    treasureMap += (3 -> "Dig.")
    println(treasureMap(2))

    //by default Map is immutable
    val romanNumeral = Map(
        1 -> "I"
      , 2 -> "II"
      , 3 -> "III"
      , 4 -> "IV"
      , 5 -> "V"
    )

  }


}
